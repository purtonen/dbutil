package dbutil

import (
	"database/sql"
	"fmt"
	"reflect"
	"strings"
	"errors"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// InitDB : Initialize database connection
func InitDb(dataSourceName string) error{
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		return err
	}

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		return err
	}	

	return nil
}

func GetRow(table string, fields map[string]string, id string) (map[string]string, error) {
	var fieldsString string
	var result map[string]string

	if db == nil {
		return nil, errors.New("Database not initialized, use 'InitDb()'")
	}

	for key := range fields {
		fieldsString += key + ","
	}
	fieldsString = strings.TrimSuffix(fieldsString, ",")

	sql := fmt.Sprintf("SELECT %s FROM %s WHERE %s_id='%s';", fieldsString, table, table, id)

	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	if !rows.Next() {
		return nil, nil
	}

	// Create a slice of interface{}'s to represent each column,
	// and a second slice to contain pointers to each item in the columns slice.
	columns := make([]interface{}, len(cols))
	columnPointers := make([]interface{}, len(cols))
	for i := range columns {
		columnPointers[i] = &columns[i]
	}

	// Scan the result into the column pointers...
	if err := rows.Scan(columnPointers...); err != nil {
		return nil, err
	}

	// Create our map, and retrieve the value for each column from the pointers slice,
	// storing it in the map with the name of the column as the key.
	for i, colName := range cols {
		val := columnPointers[i].(*interface{})
		result[fields[colName]] = fmt.Sprintf("%s", *val)
	}
	defer rows.Close()

	return result, nil
}

func GetRows(table string, fields map[string]string, limit int, page int) ([]map[string]string, error) {
	var maps []map[string]string
	var fieldsString string

	if db == nil {
		return maps, errors.New("Database not initialized, use 'InitDb()'")
	}

	offset := pageToOffset(page, limit)

	for key := range fields {
		fieldsString += key + ","
	}
	fieldsString = strings.TrimSuffix(fieldsString, ",")

	sql := fmt.Sprintf("SELECT %s FROM %s LIMIT %d OFFSET %d;", fieldsString, table, limit, offset)

	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := rows.Scan(columnPointers...); err != nil {
			continue
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]string)
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[fields[colName]] = fmt.Sprintf("%s", *val)
		}

		maps = append(maps, m)
	}
	defer rows.Close()

	return maps, nil
}

func GetAllRows(table string, fields map[string]string) ([]map[string]string, error) {
	var maps []map[string]string
	var fieldsString string

	if db == nil {
		return maps, errors.New("Database not initialized, use 'InitDb()'")
	}

	for key := range fields {
		fieldsString += key + ","
	}
	fieldsString = strings.TrimSuffix(fieldsString, ",")

	sql := fmt.Sprintf("SELECT %s FROM %s;", fieldsString, table)

	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if err := rows.Scan(columnPointers...); err != nil {
			continue
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]string)
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[fields[colName]] = fmt.Sprintf("%s", *val)
		}

		maps = append(maps, m)
	}
	defer rows.Close()

	return maps, nil
}

// rowsToInterfaces : Converts a slice of maps of values to slice of given interfaces
func RowsToInterfaces(rows []map[string]string, obj interface{}) []interface{} {

	var result []interface{}

	for _, row := range rows {
		i := RowToInterface(row, obj)
		result = append(result, i)
	}

	return result

}

// rowToInterface : Converts a map of values to given interface
func RowToInterface(row map[string]string, obj interface{}) interface{} {

	objType := reflect.TypeOf(obj)
	resObj := reflect.New(objType).Elem()

	for key, value := range row {
		field := resObj.FieldByName(key)
		if field.IsValid() && field.CanSet() {
			field.SetString(value)
		}
	}

	return resObj.Interface()

}

func pageToOffset(page int, limit int) int {
	if page == 0 || page == 1 {
		return 0
	}

	return (page - 1) * limit
}
